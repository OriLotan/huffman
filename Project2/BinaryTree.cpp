#include "BinaryTree.h"

BinaryTree::BinaryTree(Node* root) : m_root(root) {}

BinaryTree::BinaryTree() : m_root(nullptr) {}

BinaryTree::~BinaryTree()
{
	delete m_root;
}

void BinaryTree::SetRoot(Node* node)
{
	m_root = node;
}

Node* BinaryTree::GetRoot()
{
	return m_root;
}