#pragma once

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "BinaryTree.h"
#include "Node.h"
#include "Utils.h"

constexpr int ASCI_TABLE_SIZE = 128;

class HuffmanTable
{
public:

	/*
	* Line 0 : one line about the function
	*
	* @param name	what is the param
	* @return		what its return
	*/
	bool InitExtract(const char* path);

	/*
	* Line 0 : one line about the function
	*
	* @param name	what is the param
	* @return		what its return
	*/
	bool InitCompress(const char *path);

	/*
	* Line 0 : one line about the function
	*
	* @param name	what is the param
	* @return		what its return
	*/
	char* HeaderFromHuffTree();

	/*
	* Line 0 : one line about the function
	*
	* @param name	what is the param
	* @return		what its return
	*/
	bool HuffTreeFromHeader();

	/*
	* Line 0 : one line about the function
	*
	* @param name	what is the param
	* @return		what its return
	*/
	const char* CharToBinCode(char letter);

	/*
	* Line 0 : one line about the function
	*
	* @param name	what is the param
	* @return		what its return
	*/
	char BinCodeToChar(const char* binCode, size_t codeSize);

	/*
	* Line 0 : one line about the function
	*
	* @param name	what is the param
	* @return		what its return
	*/
	bool IsInit();

	/*
	* Line 0 : one line about the function
	*
	* @param name	what is the param
	* @return		what its return
	*/
	const BinaryTree* GetHuffTree();

private:
	
	/*
	* Line 0 : one line about the function
	*
	* @param name	what is the param
	* @return		what its return
	*/
	MinHeap CountLetters();
	
	/*
	* Line 0 : one line about the function
	*
	* @param name	what is the param
	* @return		what its return
	*/
	bool HuffTreeFromMinHeap(MinHeap& heap);
	
	/*
	* Line 0 : one line about the function
	*
	* @param name	what is the param
	* @return		what its return
	*/
	bool TableFromHuffTree();
	
	/*
	* Line 0 : one line about the function
	*
	* @param name	what is the param
	* @return		what its return
	*/
	bool TableFromHuffTreeRec(Node* root, char** buffer, size_t &buffSize);
	
	/*
	* Line 0 : one line about the function
	*
	* @param name	what is the param
	* @return		what its return
	*/
	void HuffTreeToStr(Node* node, char** buffer, size_t &buffSize);
	
	/*
	* Line 0 : one line about the function
	*
	* @param name	what is the param
	* @return		what its return
	*/
	bool HuffTreeFromHeaderRec(char* header, BinaryTree* root);

	char* m_table[ASCI_TABLE_SIZE];
	BinaryTree m_huffTree;
	FILE* m_file;
	bool m_isInit;
};

