#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

class Utils
{
public:
	/*
	* Line 0 : one line about the function
	*
	* @param name	what is the param
	* @return		what its return
	*/
	static bool IncreaseStrSize(char** str, size_t& length);
	
	/*
	* Line 0 : one line about the function
	*
	* @param name	what is the param
	* @return		what its return
	*/
	static bool ReadBlockFromFile(FILE* file, char* buffer, size_t size);
};