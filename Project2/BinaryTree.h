#ifndef _BINARYTREE_H_
#define _BINARYTREE_H_

#include "Node.h"

class BinaryTree
{
public:

	/*
	* C'tor
	* @param root
	*/
	BinaryTree(Node* root);

	/*
	* C'tor
	*/
	BinaryTree();

	/*
	* D'tor
	*/
	~BinaryTree();

	/*
	* Sets the root of the tree (the current node).
	* 
	* @param node The node to set to.
	* @return	void
	*/
	void SetRoot(Node* node);

	/*
	* Returns the tree's root (the current node). 
	*/
	Node* GetRoot();

private:
	Node* m_root;
};

#endif

