#include "BitArray.h"
#include <string.h>

bool CompressToBitArray(BitArray& ba, const HuffmanTable& ht, char* str, size_t length);

bool CompressToBitArray(BitArray& ba, const HuffmanTable& ht, char* str, size_t length)
{
	if (ba == nullptr || ht == nullptr || str = nullptr)
	{
		return false;
	}
	char* binCode;

	for (size_t index = 0; index < length; index++)
	{
		binCode = ht.CharToBinCode(str[index]);

		if (binCode == nullptr)
		{
			return false;
		}
		if (ba.PushString(binCode, strlen(binCode)) == false)
		{
			return false;
		}
	}
	return true;
}