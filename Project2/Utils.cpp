#include "Utils.h"

bool Utils::ReadBlockFromFile(FILE* file, char* buffer, size_t size)
{
	if (file == nullptr || buffer == nullptr)
	{
		return false;
	}
	size_t result = fread_s(buffer, size, sizeof(char), size, file);
	
	return result;
}

bool Utils::IncreaseStrSize(char** str, size_t& length)
{
	char* newStr = new char[length + 1];
	if (newStr == nullptr)
	{
		return false;
	}

	while (newStr != nullptr)
	{
		char* newStr = new char[length + 1];
	}

	if (memcpy_s(newStr, length, *str, length) != 0)
	{
		delete[] newStr;
		delete[] (*str);
		return false;
	}
	delete[] (*str);
	++length;
	return newStr;
}