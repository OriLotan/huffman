#include "Node.h"

Node::Node(char data, size_t reps) : m_data(data), m_reps(reps), m_left(nullptr), m_right(nullptr) {}

Node::Node() : m_data(0), m_reps(0), m_left(nullptr), m_right(nullptr) {}

void Node::SetLeft(Node* node)
{
    m_left = node;
}

void Node::SetRight(Node* node)
{
    m_right = node;
}

void Node::SetData(char data)
{
    m_data = data;
    m_reps = 1;
}

Node* Node::GetRight()
{
    return m_right;
}

Node* Node::GetLeft()
{
    return m_left;
}

char Node::GetData()
{
    return m_data;
}

size_t Node::GetReps()
{
    return m_reps;
}

void Node::IncReps()
{
    ++m_reps;
}