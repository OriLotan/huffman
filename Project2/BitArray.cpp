/*
* Inserts a char into the array.
* @param chr The char to insert.
* Returns if the function worked.
*/
bool PushChar(char chr);

/*
* Inserts a string into the array.
* @param str The string to insert.
* @param length The length of the string.
* Returns if the function worked.
*/
bool PushString(const char* str, size_t length);

//Gets the bit at the index
#define BIT(input, index)((input >> index) & 1)

bool PushChar(char chr)
{
	size_t bitPos = m_size * 8;

	for (size_t index = 0; index < 8; index++)
	{
		if (!SetAt(BIT(chr, index), bitPos))
		{
			return false;
		}
		++bitPos;
	}
	return true;
}

bool PushString(const char* str, size_t length)
{
	if (str == nullptr)
	{
		return false;
	}

	for (size_t index = 0; index < length; index++)
	{
		if (!PushChar(str[index]))
		{
			return false;
		}
	}
	return true;
}