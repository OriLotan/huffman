#ifndef _NODE_H_
#define _NODE_H_

class Node
{
public:

	/*
	* C'tor
	* @param data Gets the data of the node.
	*/
	Node(char data, size_t reps = 1);

	/*
	* C'tor
	*/
	Node();

	/*
	* Sets the left node.
	* @param node Gets the node to set to.
	*/
	void SetLeft(Node* node);

	/*
	* Sets the right node.
	* @param node Gets the node to set to.
	*/
	void SetRight(Node* node);

	/*
	* Sets the node's data.
	* @param data Gets the data to set to.
	*/
	void SetData(char data);

	/*
	* Returns the right node;
	*/
	Node* GetRight();

	/*
	* Returns the left node;
	*/
	Node* GetLeft();

	/*
	* Returns the node's data;
	*/
	char GetData();

	/*
	* Returns the data's number of occurrences;
	*/
	size_t GetReps();

	/*
	* Increase the number of occurrences of the data by 1. 
	*/
	void IncReps();

private:
	Node* m_left;
	Node* m_right;
	char m_data;
	size_t m_reps;
};

#endif