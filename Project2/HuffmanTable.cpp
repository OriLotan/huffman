#include "HuffmanTable.h"

constexpr char HEADER_END = '$';

bool HuffmanTable::InitExtract(const char* path)
{
    if (path == nullptr)
    {
        return false;
    }

    if (fopen_s(&m_file, path, "r") != 0)
    {
        return false;
    }

    if (!HuffTreeFromHeader())
    {
        return false;
    }

    m_isInit = true;

    return true;
}

bool HuffmanTable::InitCompress(const char* path)
{
    if (path == nullptr)
    {
        return false;
    }

    if (fopen_s(&m_file, path, "r") != 0)
    {
        return false;
    }

    MinHeap letterHeap = CountLetters();

    if (!HuffTreeFromMinHeap(letterHeap))
    {
        return false;
    }

    if (!TableFromHuffTree())
    {
        return false;
    }

    m_isInit = true;

    return true;
}

char* HuffmanTable::HeaderFromHuffTree()
{
    char* buffer = nullptr;
    size_t buffSize = 0;
    HuffTreeToStr(m_huffTree.GetRoot(), &buffer, buffSize);
    return buffer;
}


void HuffmanTable::HuffTreeToStr(Node* node, char** buffer, size_t &buffSize)
{
    if (node == nullptr)
    {
        return;
    }

    HuffTreeToStr(node->GetLeft(), buffer, buffSize);
    HuffTreeToStr(node->GetRight(), buffer, buffSize);

    char* temp = new char[++buffSize];
    if (temp == nullptr)
    {
        delete[] (*buffer);
        *buffer = nullptr;
        printf("Allocation failed!\n");
        return;
    }
    if (memcpy_s(temp, buffSize, (*buffer), buffSize - 1) != 0)
    {
        delete[](*buffer);
        *buffer = nullptr;
        printf("Copy failed!\n");
        return;
    }
    delete[] (*buffer);

    temp[buffSize - 1] = node->GetData();
    *buffer = temp;
    return;

}

const char* HuffmanTable::CharToBinCode(char letter)
{
    return m_table[letter];
}

char HuffmanTable::BinCodeToChar(const char* binCode, size_t codeSize)
{
    if (binCode == nullptr)
    {
        return -1;
    }

    Node* tree = m_huffTree.GetRoot();
    for (size_t i = 0; i < codeSize; i++)
    {
        if (binCode[i] == '0')
        {
            tree = tree->GetLeft();
        }
        else
        {
            tree = tree->GetRight();
        }
    }
    if (tree->GetLeft() == nullptr && tree->GetRight() == nullptr)
    {
        return tree->GetData();
    }

    return -1;
}

bool HuffmanTable::IsInit()
{
    return m_isInit;
}

const BinaryTree* HuffmanTable::GetHuffTree()
{
    return &m_huffTree;
}

bool HuffmanTable::HuffTreeFromHeader()
{
    return false;
}

MinHeap HuffmanTable::CountLetters()
{
    if (m_file == nullptr)
    {
        return MinHeap();
    }

    MinHeap minHeap;

    char c = 0;
    size_t CharsOccurs[ASCI_TABLE_SIZE] = { 0 };
    while ((c = fgetc(m_file)) != EOF)
    {
        ++CharsOccurs[c];
    }

    for (size_t i = 0; i < ASCI_TABLE_SIZE; i++)
    {
        if (CharsOccurs[i] > 0)
        {
            Node* newNode = new Node(static_cast<char>(i), CharsOccurs[i]);
            if (newNode == nullptr)
            {
                return MinHeap();
            }
            if (minHeap.Insert(newNode))
            {
                delete newNode;
                return MinHeap();
            }
        }
    }

    return minHeap;

}

bool HuffmanTable::HuffTreeFromMinHeap(MinHeap& heap)
{
    Node *left, *right, *top;

    if (heap.size() == 0)
    {
        return false;
    }

    top = heap.GetHead();

    while (heap.size() != 1)
    {
        left = heap.PopHead();
        right = heap.PopHead();


        top = new Node('$', left->GetReps() + right->GetReps());
        if (top == nullptr)
        {
            delete left;
            delete right;
            return false;
        }
        top->SetLeft(left);
        top->SetRight(right);

        if (!heap.Insert(top))
        {
            delete left;
            delete right;
            return false;
        }
    }

    m_huffTree = BinaryTree(top);
    return true;
}

bool HuffmanTable::TableFromHuffTree()
{
    char* buffer = nullptr;
    size_t buffSize = 0;
    return TableFromHuffTreeRec(m_huffTree.GetRoot(), &buffer, buffSize);
}

bool HuffmanTable::TableFromHuffTreeRec(Node* root, char** buffer, size_t &buffSize)
{
    if (root == nullptr)
    {
        return true;
    }

    if (root->GetLeft() == nullptr && root->GetRight() == nullptr)
    {
        m_table[root->GetData()] = new char[buffSize];
        if (m_table[root->GetData()] == nullptr)
        {
            return false;
        }
        if (memcpy_s(m_table[root->GetData()], buffSize, (*buffer), buffSize) != 0)
        {
            delete[] m_table;
            return false;
        }
        return true;
    }

    if (!(Utils::IncreaseStrSize(buffer, buffSize)))
    {
        return false;
    }

    bool recSuccess = false;
    (*buffer)[buffSize - 1] = '0';
    recSuccess = TableFromHuffTreeRec(root->GetLeft(), buffer, buffSize);
    if (!recSuccess)
    {
        return false;
    }
    (*buffer)[buffSize - 1] = '1';

    return TableFromHuffTreeRec(root->GetRight(), buffer, buffSize);
}

bool HuffmanTable::HuffTreeFromHeaderRec(char* header, BinaryTree* root)
{
    if (header[0] == '0')
    {
        if (root->GetRoot()->GetLeft() == nullptr)
        {
            Node* son = new Node('0', 0);

            if (son == nullptr)
            {
                return false;
            }
            root->GetRoot()->SetLeft(son);
            root->SetRoot(root->GetRoot()->GetLeft());
            return HuffTreeFromHeaderRec(++header, root);
        }
        else if (root->GetRoot()->GetRight() == nullptr)
        {
            Node* son = new Node('1', 0);

            if (son == nullptr)
            {
                return false;
            }
            root->GetRoot()->SetRight(son);
            root->SetRoot(root->GetRoot()->GetRight());
            return HuffTreeFromHeaderRec(++header, root);
        }
        else
        {
            return true;
        }
    }
    else if (header[0] == '1')
    {
        ++header;
        //Creates a node.
        Node* son = new Node(header[0]);

        if (son == nullptr)
        {
            return false;
        }
        if (root->GetRoot()->GetLeft() != nullptr)
        {
            if (root->GetRoot()->GetRight() != nullptr)
            {
                //Can't insert more chars acording to the tree
                return false;
            }
            else
            {
                root->GetRoot()->SetRight(son);
                return HuffTreeFromHeaderRec(++header, root);
            }
        }
        else
        {
            root->GetRoot()->SetLeft(son);
            return HuffTreeFromHeaderRec(++header, root);
        }
    }
    else if (header[0] == HEADER_END)
    {
        return true;
    }
    return false;
}

bool HuffmanTable::HuffTreeFromHeader()
{
    char* header = new char[1];
    size_t length = 1;

    for (size_t index = 0;; index++)
    {
        header[index] = fgetc(m_file);

        if (header[index] == EOF)
        {
            return false;
        }
        if (header[index] == HEADER_END)
        {
            break;
        }
        if (!(Utils::IncreaseStrSize(&header, length)))
        {
            return false;
        }
        ++length;
    }
    return HuffTreeFromHeaderRec(header, &m_huffTree);
}


bool Huffman::ExtractFromBitArray(BitArray& ba, const HuffmanTable& ht, char* buffer, size_t buffSize)
{
    char* temp = nullptr;
    size_t tempSize = 0, allocatedSize = 0;
    for (size_t i = 0, writed = 0; i < ba.GetSize(); i++)
    {
        if (tempSize == allocatedSize)
        {
            if (!Utils::IncreaseStrSize(&temp, allocatedSize))
            {
                return false;
            }
        }
        
        temp[(++tempSize) - 1] = static_cast<char>(ba.GetAt(i));

        char chr = BinCodeToChar(temp, tempSize);

        if (chr != -1)
        {
            buffer[writed++] = chr;
            if (!ba.ShiftArray(tempSize))
            {
                return false;
            }
            tempSize = 0;
        }
    }

    delete[] temp;
    return true;
}